import java.util.*;
import java.lang.*;
import java.io.*;
import org.json.*;
import com.sun.net.httpserver.*;
import java.net.*;

/* Name of the class has to be "Main" only if the class is public. */
class Share
{
	String name;
	double price;
	double num;
	double weight;
	double value;

	Share(String name_, double price_, int num_)
	{
		name = name_;
		price = price_;
		num = num_;
		value = num*price;
	}

	public boolean adjust_price(double div_value)
	{
		if(div_value >= price)
			return false;
		price -= div_value;
		return true;
	}

	public JSONObject get_info()
	{
		JSONObject temp = new JSONObject();
		temp.put("shareName", name);
		temp.put("sharePrice", price);
		temp.put("numberOfshares", num);
		temp.put("indexWeightPct", weight);
		temp.put("indexValue", value);
		return temp;
	}

	public double get_value()
	{
		return num*price;
	}

	public void adjust_num_shares(double adjust_factor)
	{
		num *= adjust_factor;
	}

	public void recalculate_weights(double index_value)
	{
		value = num*price;
		weight = value/index_value*100;
	}
}
class Index
{
	String name;
	Map<String, Share> shares;
	double index_value;
	public Index(String name_)
	{
		name = name_;
		index_value = 0;
		shares = new HashMap<String, Share>();
	}

	public int insert_share(String sname, double sprice, int snum)
	{
		if(sname == null || sname.isEmpty() || sname.trim().isEmpty())
		{
			return 400;
		}
		if(sprice <= 0)
		{
			return 400;
		}
		if(snum <= 0)
		{
			return 400;
		}
		shares.put(sname, new Share(sname, sprice, snum));
		return 0;
	}

	public void remove_share(String sname)
	{
		shares.remove(sname);
	}

	public boolean adjust_share_price(String share_name, double div_value)
	{
		Share s = shares.get(share_name);
		return s.adjust_price(div_value);
	}

	public int get_size()
	{
		return shares.size();
	}

	public boolean is_share_present(String share_name)
	{
		return shares.containsKey(share_name);
	}

	public double get_value()
	{
		return index_value;
	}

	public void set_value()
	{
		index_value = get_curr_index_value();
	}

	public JSONArray get_members()
	{
		JSONArray ja = new JSONArray();
		Set<String> keys = shares.keySet();
		for (String k : keys)
		{
			Share s = shares.get(k);
			ja.put(s.get_info());
		}
		return ja;
	}

	public double get_curr_index_value()
	{
		double ret = 0;
		Set<String> keys = shares.keySet();
		for (String k : keys)
		{
			Share s = shares.get(k);
			ret += s.get_value();
		}
		return ret;
	}

	public void adjust_num_shares(double adjust_factor)
	{
		Set<String> keys = shares.keySet();
		for (String k : keys)
		{
			Share s = shares.get(k);
			s.adjust_num_shares(adjust_factor);
		}
	}

	public void recalculate_weights()
	{
		Set<String> keys = shares.keySet();
		for (String k : keys)
		{
			Share s = shares.get(k);
			s.recalculate_weights(index_value);
		}
	}

	public void reconfigure_index(double index_val)
	{
		double val_new = get_curr_index_value();
		double adjust_factor  = index_val/val_new;
		adjust_num_shares(adjust_factor);
		recalculate_weights();
	}
}

class Test
{
	static Map<String, Index> indices;

	public static int create_index(String cmd)
	{
		JSONObject obj = new JSONObject(cmd);
		JSONObject index = obj.getJSONObject("index");
		String index_name = index.getString("indexName");
		if(index_name == null || index_name.isEmpty() || index_name.trim().isEmpty())
		{
			System.out.println("400 – if any validation fails, Index name cannot be blank or null");
			return 400;
		}
		if(indices.containsKey(index_name))
		{
			System.out.println("409 – if index already exists");
			return 409;
		}
		Index i1 = new Index(index_name);
		JSONArray arr = index.getJSONArray("indexshares");
		if(arr.length() < 2)
		{
			System.out.println("400 – if any validation fails, Each Index should have at least two members");
			return 400;
		}
		for (int i = 0; i < arr.length(); i++)
		{
			JSONObject arri = arr.getJSONObject(i);
			String share_name = arri.getString("shareName");
			double share_price = arri.getDouble("sharePrice");
			int num_shares = arri.getInt("numberOfshares");
			int success = i1.insert_share(share_name, share_price, num_shares);
			if(success!=0)
			{
				return success;
			}
		}
		i1.set_value();
		i1.reconfigure_index(i1.get_value());
		indices.put(index_name, i1);
		return 200;
	}

	public static int add_share_to_index(JSONObject add_op)
	{
		String index_name = add_op.getString("indexName");
		if(index_name == null || index_name.isEmpty() || index_name.trim().isEmpty())
		{
			return 400;
		}
		if(!indices.containsKey(index_name))
		{
			return 404;
		}
		String share_name = add_op.getString("shareName");
		Index i1 = indices.get(index_name);
		if(i1.is_share_present(share_name))
		{
			return 202;
		}
		double share_price = add_op.getDouble("sharePrice");
		int num_shares = add_op.getInt("numberOfshares");
		i1.reconfigure_index(i1.get_value()-share_price*num_shares);
		int success = i1.insert_share(share_name, share_price, num_shares);
		if(success != 0)
		{
			i1.reconfigure_index(i1.get_value());
			return success;
		}
		i1.reconfigure_index(i1.get_value());
		return 200;
	}

	public static int delete_share_from_index(JSONObject del_op)
	{
		String index_name = del_op.getString("indexName");
		if(index_name == null || index_name.isEmpty() || index_name.trim().isEmpty())
		{
			return 400;
		}
		if(!indices.containsKey(index_name))
		{
			return 404;
		}
		String share_name = del_op.getString("shareName");
		Index i1 = indices.get(index_name);
		if(!i1.is_share_present(share_name))
		{
			return 401;
		}
		if(i1.get_size() == 2)
		{
			return 405;
		}
		i1.remove_share(share_name);
		i1.reconfigure_index(i1.get_value());
		return 200;
	}

	public static int add_divident(JSONObject div_op)
	{
		String share_name = div_op.getString("shareName");
		double div_value = div_op.getDouble("dividendValue");
		if(div_value <= 0)
		{
			return 400;
		}
		boolean found = false;
		Set<String> keys = indices.keySet();
		for (String k : keys)
		{
			Index i = indices.get(k);
			if(i.is_share_present(share_name))
			{
				found = true;
				boolean succ = i.adjust_share_price(share_name, div_value);
				if(!succ)
				{
					return 400;
				}
				i.reconfigure_index(i.get_value());
			}
		}
		if(!found)
		{
			return 401;
		}
		return 200;
	}

	public static int adjust_index(String cmd)
	{
		JSONObject obj = new JSONObject(cmd);
		Iterator<String> keys = obj.keys();
		String key = keys.next();
		if(key.equals("additionOperation"))
		{
			JSONObject add_op = obj.getJSONObject("additionOperation");
			return add_share_to_index(add_op);
		}
		if(key.equals("deletionOperation"))
		{
			JSONObject del_op = obj.getJSONObject("deletionOperation");
			return delete_share_from_index(del_op);
		}
		if(key.equals("dividendOperation"))
		{
			JSONObject div_op = obj.getJSONObject("dividendOperation");
			return add_divident(div_op);
		}
		return 200;
	}

	public static JSONObject state_index(String cmd)
	{
		JSONObject ret = new JSONObject();
		Index i = indices.get(cmd);
		ret.put("indexName", cmd);
		ret.put("indexValue", i.get_value());
		ret.put("indexMembers", i.get_members());
		return ret;
	}

	public static JSONObject state_index_complete()
	{
		Set<String> keys = indices.keySet();
		JSONObject ret = new JSONObject();
		JSONArray ja = new JSONArray();
		for (String k : keys)
		{
			JSONObject temp = state_index(k);
			ja.put(temp);
		}
		ret.put("indexDetails", ja);
		return ret;
	}

	public static void main (String[] args) throws java.lang.Exception
	{
		indices = new HashMap<String, Index>();
		int port = 9000;
		HttpServer server = HttpServer.create(new InetSocketAddress(port), 0);
		System.out.println("server started at " + port);
		server.createContext("/indexState", new GetIndexStateHandler());
		server.createContext("/create", new PostCreateHandler());
		server.createContext("/indexAdjustment", new PostIndexAdjustmentHandler());
		server.setExecutor(null);
		server.start();
	}
}

class GetIndexStateHandler implements HttpHandler {

	@Override

	public void handle(HttpExchange he) throws IOException {

			JSONObject temp;
			URI requestedUri = he.getRequestURI();
            String query = requestedUri.getPath();
			String[] stringarray = query.split("/");
			if(stringarray.length > 2)
				temp = Test.state_index(stringarray[2]);
			else
				temp = Test.state_index_complete();
			// send response
			String response = temp.toString();
			he.sendResponseHeaders(200, response.length());
			OutputStream os = he.getResponseBody();
			os.write(response.toString().getBytes());
			os.close();
	}
}

class PostCreateHandler implements HttpHandler {

         @Override

         public void handle(HttpExchange he) throws IOException {
				// parse request
				InputStreamReader isr = new InputStreamReader(he.getRequestBody(), "utf-8");
				BufferedReader br = new BufferedReader(isr);
				String query = br.readLine();
				int ret = Test.create_index(query);

				// send response
				String response = "";
				he.sendResponseHeaders(ret, response.length());
				OutputStream os = he.getResponseBody();
				os.write(response.toString().getBytes());
				os.close();
         }
}

class PostIndexAdjustmentHandler implements HttpHandler {

         @Override

         public void handle(HttpExchange he) throws IOException {
				// parse request
				InputStreamReader isr = new InputStreamReader(he.getRequestBody(), "utf-8");
				BufferedReader br = new BufferedReader(isr);
				String query = br.readLine();
				int ret = Test.adjust_index(query);

				// send response
				String response = "";
				he.sendResponseHeaders(ret, response.length());
				OutputStream os = he.getResponseBody();
				os.write(response.toString().getBytes());
				os.close();
         }
}