**Instructions**

1. Run "compile_and_run.sh" to compile and run the code. 
   It will create a server on the local host port 9000.

2. Use the java executable json-java.jar as the JSON parsing library.

**Assumptions**

indexAdjustment POST request will contain single adjustment operation per request.

**Areas of improvement**

Blocking implementation, would have implemented non-blocking if had more time.
Also have not tested the corner cases.

**Testing**

I have provided input.sh script with basic test cases. 

