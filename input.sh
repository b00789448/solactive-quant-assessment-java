curl -X POST http://localhost:9000/create -d "{"index": {"indexName": "INDEX_1","indexshares": [ {"shareName": "A.OQ", "sharePrice": 10.0, "numberOfshares": 20.0},{"shareName": "B.OQ", "sharePrice": 20.0, "numberOfshares": 30.0},{"shareName": "C.OQ", "sharePrice": 30.0, "numberOfshares": 40.0},{"shareName": "D.OQ", "sharePrice": 40.0, "numberOfshares": 50.0},]}}" -o /dev/null -s -w "%{http_code}\n"
curl -X POST http://localhost:9000/indexAdjustment -d "{"additionOperation": { "shareName": "E.OQ","sharePrice": 10, "numberOfshares": 20, "indexName": "INDEX_1"}}" -o /dev/null -s -w "%{http_code}\n"
curl -X POST http://localhost:9000/indexAdjustment -d "{"deletionOperation": { "operationType": "DELETION","shareName": "D.OQ", "indexName": "INDEX_1"}}" -o /dev/null -s -w "%{http_code}\n"
curl -X POST http://localhost:9000/indexAdjustment -d "{"dividendOperation": { "operationType": "CASH_DIVIDEND", "shareName": "A.OQ", "dividendValue": 2.0}}" -o /dev/null -s -w "%{http_code}\n"
curl -X GET http://localhost:9000/indexState
echo ""
curl -X GET http://localhost:9000/indexState/INDEX_1
